import { FSEvent, FSListener, FSWatcher } from "./types";

export class Watcher implements FSWatcher {
  protected listeners: Record<FSEvent, FSListener[]> = {
    add: [],
    change: [],
    unlink: [],
    addDir: [],
    unlinkDir: [],
  };

  on(event: FSEvent, listener: FSListener) {
    let listeners = this.listeners[event];
    listeners.push(listener);
    return this;
  }

  off(event: FSEvent, listener: FSListener) {
    let listeners = this.listeners[event];
    const index = listeners.indexOf(listener);
    if (index !== -1) {
      this.listeners[event] = listeners.splice(index);
    }
    return this;
  }

  async emit(event: FSEvent, path: string) {
    const listeners = this.listeners[event];
    if (!listeners) return;

    for (const listener of listeners) {
      await listener(path);
    }
  }
}
