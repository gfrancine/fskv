export class PathManager {
  protected ROOT = "~";
  protected DELIMITER = "/";
  protected cwd = [this.ROOT];

  constructor(root?: string, delimiter?: string) {
    this.ROOT = root ? this.cleanFragment(root) : this.ROOT;
    this.DELIMITER = delimiter || this.DELIMITER;
  }

  getCWD() {
    return this.cwd;
  }

  // resolves to a relative path
  setCWD(relPath: string) {
    this.cwd = this.resolve(relPath);
  }

  // meant for use outside this class
  isRoot(fragments: string[]) {
    return fragments.length === 1 && fragments[0] === this.ROOT;
  }

  getParent(fragments: string[]) {
    return fragments.slice(0, fragments.length - 1);
  }

  // trims whitespace
  // removes a leading delimiter
  // removes a trailing delimiter
  cleanFragment(rawFragment: string) {
    let fragment = rawFragment.trim();

    if (fragment.charAt(0) === this.DELIMITER) {
      fragment = fragment.slice(1, fragment.length);
    }

    if (fragment.charAt(fragment.length - 1) === this.DELIMITER) {
      fragment = fragment.slice(0, fragment.length - 1);
    }

    return fragment;
  }

  join(...rawFragments: string[]): string {
    return rawFragments
      .map((v) => this.cleanFragment(v))
      .join(this.DELIMITER);
  }

  // leading and trailing delimiters are removed
  // when it begins with ".", it's replaced with the cwd
  // when it begins with "..", it's replaced with the cwd's parent
  // when it begins with ".." and the cwd is the root, it's replaced with the root
  // when it begins with the root, it's not modified
  // when it doesn't begin with the root, it's resolved relative to cwd
  resolve(relPath: string): string[] {
    let fragments = this.cleanFragment(relPath)
      .split(this.DELIMITER)
      .map(((v) => this.cleanFragment(v)));

    if (fragments[0] === ".") {
      fragments = [...this.cwd, ...fragments.splice(1)];
    } else if (fragments[0] === "..") {
      if (this.cwd.length === 1 && this.cwd[0] === this.ROOT) {
        fragments[0] = this.ROOT;
      } else {
        const parentFragments = this.cwd.slice(0, this.cwd.length - 1);
        fragments = [...parentFragments, ...fragments.splice(1)];
      }
    } else if (fragments[0] !== this.ROOT) {
      fragments = [...this.cwd, ...fragments];
    }

    return fragments;
  }

  // joins a resolved path
  resolveString(relPath: string): string {
    return this.join(...this.resolve(relPath));
  }
}
