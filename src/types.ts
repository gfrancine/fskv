export type Entry = {
  children?: {
    files: Record<string, 1>;
    directories: Record<string, 1>;
  };
  contents?: string;
};

export type FileEntry = Pick<Entry, "contents">;
export type TextFileEntry = Pick<Required<Entry>, "contents">;
export type DirEntry = Pick<Required<Entry>, "children">;

export type FSEvent =
  | "add"
  | "change"
  | "unlink"
  | "addDir"
  | "unlinkDir";

export type FSListener = (path: string) => void | Promise<void>;

export type FSWatcher = {
  emit(event: FSEvent, path: string): void | Promise<void>;
  on(event: FSEvent, listener: FSListener): FSWatcher;
  off(event: FSEvent, listener: FSListener): FSWatcher;
};

export type FileSystem = {
  dirExists(path: string): Promise<boolean>;
  fileExists(path: string): Promise<boolean>;
  writeTextFile(path: string, contents: string): Promise<void>;
  readTextFile(path: string): Promise<string>;
  removeFile(path: string): Promise<void>;
  createDir(path: string): Promise<void>;
  readDir(path: string): Promise<string[]>;
  removeDir(path: string): Promise<void>;

  // Path
  setCWD(path: string): Promise<void>;
  getCWD(path: string): Promise<string>;
  joinPath(...fragments: string[]): string;
  resolvePath(relPath: string): string;

  // Watcher
  watch(path: string): FSWatcher;
  unwatch(path: string): void;
};

export type KvApi<K, V> = {
  get(key: K): V | undefined | Promise<V | undefined>; // this should return a copy
  set(key: K, value: V): void | Promise<void>;
  delete(key: K): void | Promise<void>;
};
