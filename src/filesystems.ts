import { BaseFileSystem } from "./base";
import { Entry } from "./types";

export class MemoryFileSystem extends BaseFileSystem {
  entries: Record<string, Entry> = {};

  constructor() {
    super({
      api: {
        get: (key: string) => this.entries[key],
        set: (key: string, entry: Entry) => {
          this.entries[key] = entry;
        },
        delete: (key: string) => {
          delete this.entries[key];
        },
      },
    });
  }
}

export class LocalStorageFileSystem extends BaseFileSystem {
  constructor(prefix: string = "LSFILEROOT") {
    super({
      root: prefix,
      api: {
        get: (key: string) => {
          const str = localStorage.getItem(key);
          return str ? JSON.parse(str) : undefined;
        },
        set: (key: string, entry: Entry) => {
          localStorage.setItem(key, JSON.stringify(entry));
        },
        delete: (key: string) => {
          localStorage.removeItem(key);
        },
      },
    });
  }
}
