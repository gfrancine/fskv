import { MemoryFileSystem } from "./filesystems";

const fs = new MemoryFileSystem();

const debug = (c: string = ""): void => console.log(c, "\n", fs.entries);

(async () => {
  await fs.createDir("dir");
  debug("createDir");

  const makeLog = (event: string) => (path: string) => console.log(event, path);

  fs.watch("dir")
    .on("add", makeLog("add"))
    .on("unlink", makeLog("unlink"))
    .on("change", makeLog("change"))
    .on("addDir", makeLog("addDir"))
    .on("unlinkDir", makeLog("unlinkDir"));

  await fs.setCWD("dir");
  debug("setCWD");
  await fs.writeTextFile("file", "dfasf");
  debug("writeTextFile");
  await fs.removeFile("file");
  debug("removeFile");
  await fs.writeTextFile("file", "adf");
  debug("writeTextFile");

  try {
    await fs.removeDir(".");
  } catch (e) {
    console.log("successfully threw", e);
  }

  await fs.writeTextFile("file", "Asdf");
  debug("writeTextFile");
  await fs.createDir("dir2");
  debug("createDir");
  await fs.writeTextFile("./dir2/file2", "sdaf");
  debug("writeTextFile");
  await fs.setCWD("..");
  debug("setCWD");
  console.log("remove dir");
  await fs.removeDir("dir");
  debug("removeDir");
  await fs.setCWD("..");
  debug("setCWD");

  try {
    await fs.writeTextFile("./dir2/file2", "sdaf");
  } catch (e) {
    console.log("successfully threw", e);
  }

  fs.unwatch("dir");
  debug();
})();
