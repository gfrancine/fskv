import {
  DirEntry,
  Entry,
  FileEntry,
  FileSystem,
  FSEvent,
  FSWatcher,
  KvApi,
  TextFileEntry,
} from "./types";
import { PathManager } from "./paths";
import { Watcher } from "./watcher";

// returns true only when an entry has a children property
export function isDir(entry: Entry): entry is DirEntry {
  return entry.children !== undefined;
}

// returns true only when an entry has string contents
export function isTextFile(entry: Entry): entry is TextFileEntry {
  return entry.contents !== undefined;
}

// returns true only when an entry does not have children
export function isFile(entry: Entry): entry is FileEntry {
  return entry.children === undefined;
}

// a base file system that works with any KV storage
// with get, set, and delete methods
export class BaseFileSystem implements FileSystem {
  protected watchers: Record<string, FSWatcher> = {};
  protected api: KvApi<string, Entry>;
  protected paths: PathManager;

  constructor({ api, root, delimiter }: {
    api: KvApi<string, Entry>;
    root?: string;
    delimiter?: string;
  }) {
    this.api = api;
    this.paths = new PathManager(root, delimiter);
  }

  // returns false when the path doesn't exist
  // returns false when the path is a text file
  // returns true when the path is a directory
  // returns true when the path is the root
  async dirExists(relPath: string): Promise<boolean> {
    const path = this.paths.resolveString(relPath);
    if (this.paths.isRoot([path])) return true;
    const entry = await this.api.get(path);
    if (!entry) return false;
    return isDir(entry);
  }

  // resolves to a relative path
  // returns false when the path doesn't exist
  // returns false when the path is a directory
  // returns true when the path is a file
  async fileExists(relPath: string): Promise<boolean> {
    const path = this.paths.resolveString(relPath);
    const entry = await this.api.get(path);
    if (!entry) return false;
    return isFile(entry);
  }

  // DRY
  // fire all events for a path and all its parents
  // include path and pathFragments just in case it's already been computed
  private emitWatchersForPath(
    event: FSEvent,
    path: string,
    pathFragments: string[],
  ) {
    for (let i = 0; i < pathFragments.length; i++) {
      const fragmentPath = this.paths.join(...pathFragments.slice(0, i));
      const watcher = this.watchers[fragmentPath];
      if (!watcher) continue;
      watcher.emit(event, path);
    }
  }

  // DRY
  // throws when the parent directory isn't a root and doesn't exist
  private async modifyParentPath(
    pathFragments: string[],
    modifier: (parent: DirEntry) => DirEntry,
  ) {
    const parentDirFragments = this.paths.getParent(pathFragments);
    if (!this.paths.isRoot(parentDirFragments)) {
      const parentDirPath = this.paths.join(
        ...pathFragments.slice(0, pathFragments.length - 1),
      );
      const parentDir = await this.api.get(parentDirPath);
      if (!parentDir) {
        throw new Error("Path is not a child of an existing directory!");
      }
      await this.api.set(parentDirPath, modifier(parentDir as DirEntry));
    }
  }

  // throws if it's overwriting a non text file
  // writes to the file entry
  // add's to the parent's children if new
  // fires the watcher for the path
  // fires the watcher for the parent path
  // throws when the parent directory isn't a root and doesn't exist
  async writeTextFile(relPath: string, contents: string): Promise<void> {
    const pathFragments = this.paths.resolve(relPath);
    const path = this.paths.join(...pathFragments);

    const existingEntry = await this.api.get(path);
    const isNew = existingEntry === undefined;
    if (existingEntry) {
      if (!isTextFile(existingEntry)) {
        throw new Error("Path is not a text file!");
      }
    }

    const newEntry: TextFileEntry = { contents };

    if (isNew) {
      await this.modifyParentPath(pathFragments, (parentDir) => {
        parentDir.children.files[pathFragments[pathFragments.length - 1]] = 1;
        return parentDir;
      });
    }

    await this.api.set(path, newEntry);
    this.emitWatchersForPath(isNew ? "add" : "change", path, pathFragments);
  }

  // throws when the path does not exist
  // throws when the path is not a text file
  async readTextFile(relPath: string): Promise<string> {
    const path = this.paths.resolveString(relPath);
    const entry = await this.api.get(path);
    if (!entry) throw new Error("Path does not exist!");
    if (!isTextFile(entry)) throw new Error("Path is not a text file!");
    return entry.contents;
  }

  // does nothing when the path doesn't exist
  // throws when the path isn't a file;
  // fires the watcher for the path
  // fires the watcher for the parent path
  // removes the watchers for that exact path
  // throws when the parent directory isn't a root and doesn't exist
  async removeFile(relPath: string): Promise<void> {
    const pathFragments = this.paths.resolve(relPath);
    const path = this.paths.join(...pathFragments);

    const entry = await this.api.get(path);
    if (!entry) return;

    if (!isFile(entry)) throw new Error("Path is not a file!");

    await this.modifyParentPath(pathFragments, (parentDir) => {
      delete parentDir.children.files[pathFragments[pathFragments.length - 1]];
      return parentDir;
    });

    await this.api.delete(path);
    this.emitWatchersForPath("unlink", path, pathFragments);
  }

  // throws if a non-empty dir already exists
  // throws if path is a file
  // adds to the parent dir's children
  // overwrites an existing directory if it's empty
  // throws when the parent directory isn't a root and doesn't exist
  async createDir(relPath: string): Promise<void> {
    const pathFragments = this.paths.resolve(relPath);
    const path = this.paths.join(...pathFragments);
    const existingEntry = await this.api.get(path);

    if (existingEntry) {
      if (
        isDir(existingEntry) &&
        existingEntry.children.files.length > 0 &&
        existingEntry.children.directories.length > 0
      ) {
        throw new Error("Path is a non-empty directory!");
      } else if (isFile(existingEntry)) {
        throw new Error("Path is an existing file!");
      }
    }

    const newEntry: DirEntry = {
      children: { files: {}, directories: {} },
    };

    await this.modifyParentPath(pathFragments, (parentDir) => {
      parentDir.children.directories[pathFragments[pathFragments.length - 1]] =
        1;
      return parentDir;
    });

    await this.api.set(path, newEntry);
    this.emitWatchersForPath("addDir", path, pathFragments);
  }

  // returns the paths of its children
  // throws when the path does not exist
  // throws when the parent directory isn't a root and doesn't exist
  async readDir(relPath: string): Promise<string[]> {
    const path = this.paths.resolveString(relPath);
    const entry = await this.api.get(path);

    if (!entry) throw new Error("Path does not exist!");
    if (!isDir(entry)) throw new Error("Path is not a directory!");

    return Object.keys(entry.children);
  }

  // removes its children recursively
  // fires all the watchers for its children
  // throws if we're trying to remove the cwd
  async removeDir(relPath: string): Promise<void> {
    const pathFragments = this.paths.resolve(relPath);
    const path = this.paths.join(...pathFragments);

    if (path === this.paths.join(...this.paths.getCWD())) {
      throw new Error("Cannot remove current working directory!");
    }

    const entry = await this.api.get(path);
    if (!entry) return;
    if (!isDir(entry)) throw new Error("Path is not a directory!");

    await this.modifyParentPath(pathFragments, (parentDir) => {
      delete parentDir.children
        .directories[pathFragments[pathFragments.length - 1]];
      return parentDir;
    });

    await this.api.delete(path);

    // use a stack based walk
    let pathStack = Object.keys(entry.children.files)
      .concat(Object.keys(entry.children.directories))
      .map((name) => this.paths.join(...pathFragments, name));

    while (pathStack.length > 0) {
      const childPath = pathStack.pop() as string;
      const childEntry = this.api.get(childPath) as Entry;
      const childPathFragments = this.paths.resolve(childPath);

      if (isDir(childEntry)) {
        pathStack = pathStack.concat(
          Object.keys(childEntry.children.files)
            .concat(Object.keys(childEntry.children.directories))
            .map((name) => this.paths.join(...childPathFragments, name)),
        );
        await this.api.delete(childPath);
        this.emitWatchersForPath("unlinkDir", childPath, childPathFragments);
      } else if (isFile(childEntry)) {
        await this.api.delete(childPath);
        this.emitWatchersForPath("unlink", childPath, childPathFragments);
      }
    }

    // only emit when it's all done
    this.emitWatchersForPath("unlinkDir", path, pathFragments);
  }

  // Path

  // throws when the path isn't a valid directory
  async setCWD(relPath: string): Promise<void> {
    if (!(await this.dirExists(relPath))) {
      throw new Error("Path is not a directory!");
    }
    return this.paths.setCWD(relPath);
  }

  getCWD(): Promise<string> {
    return Promise.resolve(this.paths.join(...this.paths.getCWD()));
  }

  joinPath(...rawFragments: string[]) {
    return this.paths.join(...rawFragments);
  }

  resolvePath(relPath: string) {
    return this.paths.resolveString(relPath);
  }

  // Watcher

  watch(relPath: string): FSWatcher {
    const path = this.paths.resolveString(relPath);
    let watcher = this.watchers[path];
    if (watcher) return watcher;
    watcher = new Watcher();
    this.watchers[path] = watcher;
    return watcher;
  }

  unwatch(relPath: string) {
    const path = this.paths.resolveString(relPath);
    let watcher = this.watchers[path];
    if (watcher) delete this.watchers[path];
  }
}
